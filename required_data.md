# Required data

- Provider: liquidity provider;
- Pair: base currency / quote currency;
- Date: transaction datetime;
- Price: benchmark price;
- Quantity: quantity of the currency;
- Market Data Link: arbitrary link.

## Endpoints

- Market Data: return the records of **one or more** `Providers` to **a chosen** `Pair` within the **defined** `Datetime` **range, including** `Price` and `Quantity` to calculate the VWAP;
- Blotter graph: for the (`Provider`, `Datetime`) **tupĺe(s) chosen** , return the respective **price and quantity in addition to them**;
- Blotter list: for the **chosen** `Pair(s)` within the `Datetime` **required range** and the `Price` and `Quantity` **optional ranges**, return a **list** with the `Market Data Link` **in addition to them**.
