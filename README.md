# [Palladris](https://www.palladris.com/) JSON API prototype

Implementation of a JSON API web server that will provide the data for the specific mockup pages.

First, install all the requirements, clone the repository and then build and run the web server.

This template can be used to generate a full-stack web application using the [SAFE Stack](https://safe-stack.github.io/). It was created using the dotnet [SAFE Template](https://safe-stack.github.io/docs/template-overview/). If you want to learn more about the template why not start with the [quick start](https://safe-stack.github.io/docs/quickstart/) guide?

## Install pre-requisites

You'll need to install the following pre-requisites in order to build SAFE applications

### The [.NET Core SDK](https://www.microsoft.com/net/download) (>= 3.0)

```shell
wget https://packages.microsoft.com/config/ubuntu/19.10/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
sudo apt update
sudo apt install apt-transport-https
sudo apt update
sudo apt install dotnet-sdk-3.1
```

### The [Yarn](https://yarnpkg.com/lang/en/docs/install/) (>= 1.10.1 and < 2) package manager (you can also use `npm` but the usage of `yarn` is encouraged) and [Node LTS](https://nodejs.org/en/download/) (>= 8.0) for the front end components

```shell
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add - 
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update && sudo apt install yarn
```

### [Mono](https://www.mono-project.com/docs/getting-started/install/)

```shell
sudo apt install gnupg ca-certificates
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
echo "deb https://download.mono-project.com/repo/ubuntu stable-bionic main" | sudo tee /etc/apt/sources.list.d/mono-official-stable.list
sudo apt update
sudo apt install mono-complete
```

## Clone the repository

```shell
sudo apt install git
git clone https://gitlab.com/ShakaWaka/palladris-json-api-prototype.git
```

## Work with the application

Before you run the project **for the first time only** you should install its local tools with this command:

```shell
dotnet tool restore
```

To concurrently run the server and the client components in watch mode use the following command:

```shell
dotnet fake build -t run
```

## SAFE Stack Documentation

You will find more documentation about the used F# components at the following places:

* [Saturn](https://saturnframework.org/docs/)
* [Fable](https://fable.io/docs/)
* [Elmish](https://elmish.github.io/elmish/)
* [Fulma](https://fulma.github.io/Fulma/)

If you want to know more about the full Azure Stack and all of it's components (including Azure) visit the official [SAFE documentation](https://safe-stack.github.io/docs/).
