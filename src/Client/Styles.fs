module Client.Styles

open Fable.React.Props
open Fable.Core.JsInterop
open Fable.Import
open Elmish.Navigation
open Fable.React

let goToUrl (e: Browser.Types.MouseEvent) =
    e.preventDefault()
    let href = !!e.target?href
    Navigation.newUrl href |> List.map (fun f -> f ignore) |> ignore

let viewLink page description =
    a [ Style [ Padding "0 20px" ]
        Href (Pages.toPath page)
        OnClick goToUrl ]
        [ str description ]

let centerStyle direction =
    Style [ Display DisplayOptions.Flex
            FlexDirection direction
            AlignItems AlignItemsOptions.Center
            JustifyContent "center"
            Padding "20px 0"
    ]

let logoutLink onClick description =
    a [ Style [ Padding "0 20px" ]
        OnClick (fun _ -> onClick())
        OnTouchStart (fun _ -> onClick()) ]
        [ str description ]

let errorBox msg =
    div [] [
        match msg with
        | None -> ()
        | Some e -> yield p [ClassName "text-danger"][str e]
    ]

let onEnter msg dispatch =
    function
    | (ev:Browser.Types.KeyboardEvent) when ev.keyCode = 13. ->
        ev.preventDefault()
        dispatch msg
    | _ -> ()
    |> OnKeyDown