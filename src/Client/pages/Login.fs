module Client.Login

open Elmish
open Fable.React
open Fable.React.Props
open ServerCode.Domain
open System
open Fable.Core.JsInterop
open Fetch.Types
open Fulma
open Client.Styles
open Client.Utils
#if FABLE_COMPILER
open Thoth.Json
#else
open Thoth.Json.Net
#endif

// The model holds data that you want to keep track of while the application is running
// in this case, we are keeping track of a counter
// we mark it as optional, because initially it will not be available from the client
// the initial value will be requested from server
type Model = { 
    Login : Login
    Running : bool
    ErrorMsg : string option }

// The Msg type defines what events/actions can occur while the application is running
// the state of the application changes *only* in reaction to these events
type Msg =
    | LoginSuccess of UserData
    | SetEmail of string
    | SetPassword of string
    | AuthError of exn
    | LogInClicked

let authUser (login:Login) = promise {
    if String.IsNullOrEmpty login.Email then return! failwithf "You need to fill in a email." else
    if String.IsNullOrEmpty login.Password then return! failwithf "You need to fill in a password." else

    let body = Encode.Auto.toString(0, login)

    let props = [
        Method HttpMethod.POST
        Fetch.requestHeaders [ ContentType "application/json" ]
        Body !^body
    ]

    try
        let! res = Fetch.fetch "/api/users/login/" props
        let! txt = res.text()
        return Decode.Auto.unsafeFromString<UserData> txt
    with _ ->
        return! failwithf "Could not authenticate user."
}

// defines the initial state and initial command (= side-effect) of the application
let init (user:UserData option) =
    let email = user |> Option.map (fun u -> u.Email) |> Option.defaultValue ""

    { Login = { Email = email; Password = ""; PasswordId = Guid.NewGuid() }
      Running = false
      ErrorMsg = None }, Cmd.none

// The update function computes the next state of the application based on the current state and the incoming events/messages
// It can also run side-effects (encoded as commands) like calling the server via Http.
// these commands in turn, can dispatch messages to which the update function will react.
let update (msg:Msg) model : Model*Cmd<Msg> =
    match msg with
    | LoginSuccess _ ->
        model, Cmd.none

    | SetEmail email ->
        { model with Login = { model.Login with Email = email; Password = ""; PasswordId = Guid.NewGuid() } }, Cmd.none

    | SetPassword pw ->
        { model with Login = { model.Login with Password = pw }}, Cmd.none

    | LogInClicked ->
        { model with Running = true },
            Cmd.OfPromise.either authUser model.Login LoginSuccess AuthError

    | AuthError exn ->
        { model with Running = false; ErrorMsg = Some exn.Message }, Cmd.none


let safeComponents =
    let components =
        span [ ]
           [ a [ Href "https://github.com/SAFE-Stack/SAFE-template" ]
               [ str "SAFE  "
                 str Version.template ]
             str ", "
             a [ Href "https://saturnframework.github.io" ] [ str "Saturn" ]
             str ", "
             a [ Href "http://fable.io" ] [ str "Fable" ]
             str ", "
             a [ Href "https://elmish.github.io" ] [ str "Elmish" ]
             str ", "
             a [ Href "https://fulma.github.io/Fulma" ] [ str "Fulma" ]
             str ", "
             a [ Href "https://bulmatemplates.github.io/bulma-templates/" ] [ str "Bulma\u00A0Templates" ]

           ]

    span [ ]
        [ str "Version "
          strong [ ] [ str Version.app ]
          str " powered by: "
          components ]

type Props = {
    Model: Model
    Dispatch: Msg -> unit
}

let view = elmishView "Login" <| fun { Model = model; Dispatch = dispatch } ->
    Column.column
        [ Column.Width (Screen.All, Column.Is4)
          Column.Offset (Screen.All, Column.Is4) ]
        [ Heading.h3
            [ Heading.Modifiers [ Modifier.TextColor IsGrey ] ]
            [ str "Login" ]
          Heading.p
            [ Heading.Modifiers [ Modifier.TextColor IsGrey ] ]
            [ str "Please login to proceed." ]
          Box.box' [ ]
            [ figure [ Class "avatar" ]
                [ img [ Src "https://placehold.it/128x128" ] ]
              errorBox model.ErrorMsg 
              form [ ]
                [ Field.div [ ]
                    [ Control.div [ ]
                        [ Input.email
                            [ Input.Id "email"
                              Input.Size IsLarge
                              Input.Placeholder "Email"
                              Input.DefaultValue model.Login.Email
                              Input.OnChange (fun ev -> dispatch (SetEmail ev.Value))
                              Input.Props [ AutoFocus true ] ] ] ]
                  Field.div [ ]
                    [ Control.div [ ]
                        [ Input.password
                            [ Input.Id "password"
                              Input.Key ("password_" + model.Login.PasswordId.ToString())
                              Input.Size IsLarge
                              Input.Placeholder "Your Password"
                              Input.DefaultValue model.Login.Password
                              Input.OnChange (fun ev -> dispatch (SetPassword ev.Value))
                              // onEnter LogInClicked dispatch 
                              ] ] ]
                  // counter model dispatch
                  // Field.div [ ]
                  //   [ Checkbox.checkbox [ ]
                  //       [ input [ Type "checkbox" ]
                  //         str "Remember me" ] ]
                  Button.button
                    [ Button.Color IsInfo
                      Button.IsFullWidth
                      Button.CustomClass "is-large is-block"
                      Button.OnClick (fun _ -> dispatch LogInClicked) ]
                    [ str "Login" ] ] ]
          Text.p [ Modifiers [ Modifier.TextColor IsGrey ] ]
            [ a [ ] [ str "Sign Up" ]
              str "\u00A0·\u00A0"
              a [ ] [ str "Forgot Password" ]
              str "\u00A0·\u00A0"
              a [ ] [ str "Need Help?" ] ]
          br [ ]
          Text.div [ Modifiers [   Modifier.TextColor IsGrey ] ]
            [ safeComponents ] ]
