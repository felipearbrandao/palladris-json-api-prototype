module Client.Report

open Fable.React
open Fable.React.Props
open Fable.Core.JsInterop

open Elmish
open Fetch.Types
open ServerCode
open ServerCode.Domain
open Client.Styles
open Client.Utils
open System
#if FABLE_COMPILER
open Thoth.Json
#else
open Thoth.Json.Net
#endif

type Model = {
    // Domain data
    Report : Report
    // Additional view data
    Token : string
    ErrorMsg : string option
}

/// The different messages processed when interacting with the market data
type Msg =
    | FetchedReport of Report
    | FetchError of exn

/// Get the report from the server, used to populate the model
let getReport email =
    promise {
        let url =
            match dataType with
            | MarketData -> sprintf "/api/marketdata/%s" email
            | BlotterChart -> sprintf "/api/blotterchart/%s" email
            | BlotterTable -> sprintf "/api/blottertable/%s" email
          
        let props = [ ]

        let! res = Fetch.fetch url props
        let! txt = res.text()
        return Decode.Auto.unsafeFromString<Report> txt
    }

let init (email:string) (dataType:Data) (token:string) =
    { Report = Report.New email
      Token = token
      ErrorMsg = None },
        Cmd.batch [
            Cmd.OfPromise.either getReport email FetchedReport FetchError ]

let update (msg:Msg) model : Model * Cmd<Msg> =
    match msg with
    | FetchedReport report ->
        let report = { report with Transactions = report.Transactions |> List.sortBy (fun b -> b.Date) }
        { model with Report = report }, Cmd.none

    | FetchError e ->
        { model with ErrorMsg = Some e.Message }, Cmd.none
