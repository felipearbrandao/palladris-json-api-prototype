module Client.Menu

open Fable.React
open Client.Styles
open Client.Pages
open ServerCode.Domain
open Client.Utils

type Model = {
    User : UserData option
    RenderedOnServer : bool
}

type Props = {
    Model : Model
    OnLogout : unit -> unit
}

let view = elmishView "Menu" (fun (props:Props) ->
    div [ centerStyle "row" ] [
        match props.Model.User with
        | Some _ ->
            yield logoutLink props.OnLogout "Logout"
        | _ ->
            yield viewLink Page.Login "Login"

        if props.Model.RenderedOnServer then
            yield str " - Rendered on server"
        else
            yield str " - Rendered on client"
    ]
)