module Client.Shared

open ServerCode.Domain

type PageModel =
    | LoginModel of Login.Model
    | NotFoundModel
    | ReportModel of Report.Model

type Model = {
    MenuModel : Menu.Model
    PageModel : PageModel
}

/// The composed set of messages that update the state of the application
type Msg =
    | AppHydrated
    | LoginMsg of Login.Msg
    | LoggedIn of UserData
    | LoggedOut
    | ReportMsg of Report.Msg
    | StorageFailure of exn
    | Logout of unit


// VIEW

open Fable.React
open Fable.React.Props
open Client.Styles

let view model dispatch =
    div [ Key "Application" ] [
        Menu.view { Model = model.MenuModel; OnLogout = (Logout >> dispatch) }
        hr []

        div [ centerStyle "column" ] [
            match model.PageModel with
            | LoginModel m ->
                yield Login.view { Model = m; Dispatch = (LoginMsg >> dispatch) }
            | NotFoundModel ->
                yield div [] [ str "The page is not available." ]
            | ReportModel m ->
                yield div [] [ str "200 OK." ]
        ]
    ]