module Client.Pages

open Elmish.UrlParser

/// The different pages of the application. If you add a new page, then add an entry here.
[<RequireQualifiedAccess>]
type Page =
    | NotFound
    | Login
    | MarketData
    | BlotterChart
    | BlotterTable


let toPath =
    function
    | Page.NotFound -> "/notfound"
    | Page.Login -> "/"
    | Page.MarketData -> "/marketdata"
    | Page.BlotterChart -> "/blotterchart"
    | Page.BlotterTable -> "/blottertable"


/// The URL is turned into a Result.
let pageParser : Parser<Page -> Page, _> =
    oneOf
        [ map Page.NotFound (s "notfound")
          map Page.Login (s "")
          map Page.MarketData (s "marketdetata")
          map Page.BlotterChart (s "blotterchart")
          map Page.BlotterTable (s "blottertable") ]

let urlParser location = parsePath pageParser location
