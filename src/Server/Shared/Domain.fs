/// Domain model shared between client and server.
namespace ServerCode.Domain

open System

// Json web token type.
type JWT = string

// Login credentials.
type Login =
    { Email   : string
      Password   : string
      PasswordId : Guid }

    member this.IsValid() =
        not ((this.Email <> "test"  || this.Password <> "test") &&
             (this.Email <> "test2" || this.Password <> "test2"))

type UserData =
    { Email : string
      Token : JWT }

/// The data for each transaction
type Transaction =
    { Provider : string
      Pair : string
      Date : DateTime
      Price : float
      Quantity : int
      MarketDataLink : string }

    static member New provider pair date price quantity marketDataLink =
        { Provider = provider
          Pair = pair
          Date = date
          Price = price
          Quantity = quantity
          MarketDataLink = marketDataLink }
type Data =
    | MarketData
    | BlotterChart
    | BlotterTable

type Report =
    { Email : string
      Transactions : Transaction list }

    static member New email =
        { Email = email
          Transactions = [] }
