module ServerCode.Defaults

open ServerCode.Domain
open System

/// The default initial data
let defaultReport email =
    { Email = email
      Transactions =
            [ { Provider = "CBOE"
                Pair = "EUR/GBP"
                Date = new DateTime(2018, 10, 20, 21, 32, 00)
                Price = 1.2979
                Quantity = 1000000
                MarketDataLink = "https://www.britishcouncil.cn"}
              { Provider = "UBS"
                Pair = "EUR/GBP"
                Date = new DateTime(2018, 10, 21, 21, 46, 00)
                Price = 1.2981
                Quantity = 1500000
                MarketDataLink = "https://whatwg.org" }
              { Provider = "UBS"
                Pair = "EUR/USD"
                Date = new DateTime(2018, 10, 22, 21, 59, 00)
                Price = 1.2983
                Quantity = 2000000
                MarketDataLink = "https://www.rhodeshouse.ox.ac.uk" }
              { Provider = "CBOE"
                Pair = "EUR/USD"
                Date = new DateTime(2018, 10, 23, 22, 12, 00)
                Price = 1.2985
                Quantity = 2500000
                MarketDataLink = "https://skybluefc.com" }
              { Provider = "CBOE"
                Pair = "GBP/USD"
                Date = new DateTime(2018, 10, 24, 22, 25, 00)
                Price = 1.2987
                Quantity = 3000000
                MarketDataLink = "https://www.curlingbasics.com" }
              { Provider = "UBS"
                Pair = "GBP/USD"
                Date = new DateTime(2018, 10, 25, 22, 38, 43)
                Price = 1.2989
                Quantity = 3500000
                MarketDataLink = "https://www.valdoise.fr" } ]
    }