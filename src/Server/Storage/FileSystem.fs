module ServerCode.Storage.FileSystem

open System.IO
open ServerCode
open ServerCode.Domain
open Thoth.Json.Net

/// Get the file name used to store the data for a specific user
let getJSONFileName email = sprintf "./temp/db/%s.json" email

let getReportFromDB email =
    let fi = FileInfo(getJSONFileName email)
    if not fi.Exists then Defaults.defaultReport email
    else
        File.ReadAllText(fi.FullName)
        |> Decode.Auto.unsafeFromString<Report>
