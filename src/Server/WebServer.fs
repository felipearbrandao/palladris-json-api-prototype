/// Functions for managing the Giraffe web server.
module ServerCode.WebServer

open ServerCode
open Giraffe
open Saturn.Router

/// Start the web server and connect to database
let webApp databaseType =
    let db = Database.getDatabase databaseType


    let secured = router {
        pipe_through (Saturn.Auth.requireAuthentication Saturn.ChallengeType.JWT)
    }

    router {
        not_found_handler Pages.notfound

        post "/api/users/login/" Auth.login
        getf "/api/marketdata/%s" (Report.getReport db.LoadReport)
        getf "/api/blotterchart/%s" (Report.getReport db.LoadReport)
        getf "/api/blottertable/%s" (Report.getReport db.LoadReport)

        // SSR
        get "" Pages.login
        get "/" Pages.login
        get "/login" Pages.login

        forward "" secured
    }