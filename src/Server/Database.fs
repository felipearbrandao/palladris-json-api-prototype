/// Functions for managing the database.
module ServerCode.Database

open ServerCode
open System.Threading.Tasks

[<RequireQualifiedAccess>]
type DatabaseType =
    | FileSystem

type IDatabaseFunctions =
    abstract member LoadReport : string -> Task<Domain.Report>

/// Start the web server and connect to database
let getDatabase databaseType =
    match databaseType with
    | DatabaseType.FileSystem ->
        { new IDatabaseFunctions with
            member __.LoadReport key = Task.FromResult (Storage.FileSystem.getReportFromDB key)
        }