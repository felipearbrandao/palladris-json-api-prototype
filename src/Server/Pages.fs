module ServerCode.Pages

open Giraffe
open Client.Shared
open FSharp.Control.Tasks.ContextInsensitive
open System.Threading.Tasks
open Microsoft.AspNetCore.Http

let login: HttpHandler = fun next ctx ->
    task {
        let model: Model = {
            MenuModel ={ User = None; RenderedOnServer = true }
            PageModel =
                let m, _ = Client.Login.init None
                LoginModel m
        }
        return! next ctx
    }

let notfoundModel: Model = {
    MenuModel = { User = None; RenderedOnServer = true }
    PageModel = NotFoundModel
}

let notfound = fun next (ctx : HttpContext) ->
    ctx.SetStatusCode 404
    text  "Not found" next ctx