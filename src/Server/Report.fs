/// Report API web parts and data access functions.
module ServerCode.Report

open System.Threading.Tasks
open Microsoft.AspNetCore.Http
open Giraffe
open Microsoft.Extensions.Logging
open ServerCode.Domain
open FSharp.Control.Tasks.ContextInsensitive
open Saturn.ControllerHelpers

/// Handle the GET on /api/report
let getReport (getReportFromDB : string -> Task<Report>) (email:string) _next (ctx: HttpContext) =
    task {
        try
            let! report = getReportFromDB email
            return! ctx.WriteJsonAsync report
        with exn ->
            let msg = "Database not available"
            let logger = ctx.GetLogger "report"
            logger.LogError (EventId(), exn, msg)
            return! Response.serviceUnavailable ctx "Database not available"
    }