/// Server program entry point module.
module ServerCode.Server

open System
open System.IO
open Microsoft.Extensions.DependencyInjection
open Giraffe
open Saturn.Application
open ServerCode
open Thoth.Json.Giraffe
open Microsoft.Extensions.Logging
open ServerCode.Database

let tryGetEnv = System.Environment.GetEnvironmentVariable >> function null | "" -> None | x -> Some x

let publicPath = Path.GetFullPath "../Client/public"

let getPortsOrDefault defaultVal =
    match Environment.GetEnvironmentVariable("GIRAFFE_FABLE_PORT") with
    | null -> defaultVal
    | value -> value |> uint16

let serviceConfig (services : IServiceCollection) =
    services.AddSingleton<Serialization.Json.IJsonSerializer>(ThothSerializer())

[<EntryPoint>]
let main args =
    try
        let args = Array.toList args
        let clientPath =
            match args with
            | clientPath:: _  when Directory.Exists clientPath -> clientPath
            | _ ->
                // did we start from server folder?
                let devPath = Path.Combine("..","Client")
                if Directory.Exists devPath then devPath
                else
                    // maybe we are in root of project?
                    let devPath = Path.Combine("src", "Client")
                    if Directory.Exists devPath then devPath
                    else @"./client"
            |> Path.GetFullPath

        let database = DatabaseType.FileSystem

        let port = getPortsOrDefault 8085us

        let app = application {
            use_router (WebServer.webApp database)
            url ("http://0.0.0.0:" + port.ToString() + "/")

            use_jwt_authentication JsonWebToken.secret JsonWebToken.issuer
            memory_cache
            use_json_serializer(ThothSerializer())
            service_config serviceConfig
            use_static clientPath
            use_gzip
        }
        run app
        0
    with
    | exn ->
        let color = Console.ForegroundColor
        Console.ForegroundColor <- ConsoleColor.Red
        Console.WriteLine(exn.Message)
        Console.ForegroundColor <- color
        1
